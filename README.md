Boilerplate docker-compose scaffold for personal projects. Grep for "boilerplate" to match your setup.

A `docker-compose build` will build the stack.

To run in production mode, consider using `docker-compose -f docker-compose.yml up`, which will use the `docker-compose.yml` file and nothing else.

To run in development mode, consider using `docker-compose up`, which will overlay the normal `docker-compose.yml` with everything in `docker-compose.override.yml`, in our case bind-mounting our code directories over the container's, and starting up servers in debug mode where applicable. On a tangent, you'll probably want to run `docker-compose run app_nodejs npm install` first, since we're overlaying the source directory in the container (which had it's NPM modules installed during `build`) with the ones on our disk, which has no NPM modules installed.

You could extend this with other files which overlay `docker-compose.yml` (say a production-specific one). In this case, as per the docker-compose docs, run `docker-compose -f docker-compose.yml -f my_override_file some_operation`.

Browsing to `http://localhost:8001/app_flask` or `/app_nodejs` will, hopefully, show you some output.

There's a file called `env` which has environment variables handed down to all the containers (via `docker-compose.yml`), you can use this for shared configuration. There's also a file called `.env`, it contains a variable called COMPOSE_PROJECT_NAME, which docker-compose will use as a prefix for any created volumes/networks/images. I use this to clearly separate production/testing/development instances running on the same machine.

### web

Runs a standard nginx with `/web/default.conf` as its config, serving from `/web/www`, listening on localhost:8001.

The recommended way to go is to put a reverse proxy in front of this, and SSL-terminate on the reverse proxy. If you're going down this route with nginx, throw the file `config/relevant_nginx_site.conf` in your server's `/etc/nginx/sites-enabled`, update the hostname, and if so inclined install certbot-nginx and set it up to use SSL with `certbot --nginx`.

### apps

#### app_flask

A placeholder flask(_restful) app. Any calls will kick off the worker, see below.

#### app_nodejs

A placeholder nodejs/express app.

### db

Runs a standard MySQL as UTF-8 as possible, with:
* `/env` containing the auth credentials.
* its database contents living in a docker volume.
* `/db/backups/` where it saved/loads its backups (see *backups* below).
* `/db/entrypoint/` where it looks for first-run SQL and shell scripts (see MySQL docker docs).
* `/env` containing the auth credentials.

#### backups

`/scripts/db_backup.sh` dumps the MySQL contents to a timestamped file in `/db/backups/`, and returns the full path to it for your cronning pleasure.

`/scripts/db_restore.sh` takes a backup's filename (which you need to place in `/db/backups/` first) and reloads the database from that.

#### misc

`/scripts/db_shell.sh` loads up a MySQL shell within the running db instance.

### queue

A standard Redis.

### worker

A placeholder worker to handle longer-living stuff (like sending mails) asyncly. Dies randomly so that the retry mechanism kicks in.

### in closing

We had fucking Plan9 and now we're dealing with _this_.
