#!/bin/sh

OUT=`date --iso-8601=seconds`.sql
DDIR=`dirname $0`/../

. $DDIR/env
docker-compose -f $DDIR/docker-compose.yml exec db sh -c 'exec echo "[client]
host=localhost
user=root
password='$MYSQL_ROOT_PASSWORD'" > /root/credentials.txt'

docker-compose -f $DDIR/docker-compose.yml exec db mysqldump --defaults-extra-file=/root/credentials.txt --all-databases --single-transaction --result-file=/backups/$OUT

echo `readlink -e $DDIR/db/backups/$OUT`
